import matplotlib.pyplot as plt
import numpy as np
from copy import deepcopy




## =========== Main =========== ##

n = 5
d = 3
max = n**3

M = create_M(n, max)

# M = np.array([[ 8, 24,  6, 12,  5],
#               [19, 25,  9, 10, 18],
#               [16, 10, 23, 25,  5],
#               [22, 20, 14, 12, 15],
#               [21, 24, 15, 16,  5]])

max_SMRC_conj, R_conj, C_conj = get_max_SMRC_with_conjecture(M, d)

print("max_SMRC_conj =", max_SMRC_conj)


max_SMRC_true, R_true, C_true = get_true_max_SMRC(M, d)

print("max_SMRC_true =", max_SMRC_true)


## =========== Functions: Resolution with a conjecture =========== ##

def create_M(n, max):
    """This function creates a n*n matrix with random
    coefficients in {1, ..., max}."""
    
    n_mat = (max + 1) * np.ones((n,n))
    M = np.random.randint(n_mat)
    
    return M


def sum_d_biggest_values_row(M, d):
    """This function returns a vector made of the sums of each row,
    but each sum is made only on the d biggest values of the row."""
    
    M_copy = deepcopy(M)
    M_copy.sort(axis = 1)

    return np.sum(M_copy[:,-d:], axis = 1)


def create_rows_dictionary(M, d):
    """This function creates a kind of dictionary:
    each row of M is linked to the sum of its d biggest values.
    
    The output is a list of lists [s, r] where r is the
    row number and s is the sum of the d biggest values of the row."""
    
    n = len(M)
    rows_dict = [ [-1, r] for r in range(n) ]
    
    # M_rows_sum = np.sum(M, axis = 1)
    M_rows_sum = sum_d_biggest_values_row(M, d)
    
    for i in range(n):
        rows_dict[i][0] = M_rows_sum[i]
    
    return rows_dict


def sum_d_biggest_values_col(M, d):
    """This function returns a vector made of the sums of each column,
    but each sum is made only on the d biggest values of the column."""
    
    M_copy = deepcopy(M)
    M_copy.sort(axis = 0)

    return np.sum(M_copy[-d:,:], axis = 0)


def create_columns_dictionary(M, d):
    """This function creates a kind of dictionary:
    each column of M is linked to the sum of its d biggest values.
    
    The output is a list of lists [s, c] where c is the
    column number and s is the sum of the d biggest values of the row."""
    
    n = len(M)
    cols_dict = [ [-1, c] for c in range(n) ]
    
    # M_cols_sum = np.sum(M, axis = 0)
    M_cols_sum = sum_d_biggest_values_col(M, d)
    
    for i in range(n):
        cols_dict[i][0] = M_cols_sum[i]
    
    return cols_dict


def sort_dictionary(dic):
    
    dic.sort(reverse = True)


def get_R_and_C_with_conjecture(M, d):
    
    rows_dict = create_rows_dictionary(M, d)
    cols_dict = create_columns_dictionary(M, d)
    
    sort_dictionary(rows_dict)
    sort_dictionary(cols_dict)
    
    R = np.array([ [ rows_dict[i][1]    for i in range(d) ] ] )
    C = np.array([ [ cols_dict[i][1] ]  for i in range(d) ]   )
    
    return (R, C)
    
    
def get_submatrix(M, R, C, d):
    """This function returns a submatrix of M, whose
    coefficients are the intersections of R and C."""
    
    temporary_R_mat = np.repeat(R, d, axis = 0)
    temporary_C_mat = np.repeat(C, d, axis = 1)
    
    return M[temporary_R_mat, temporary_C_mat]


def get_SMRC(M, R, C, d):
    """This function returns the S_M(R,C)."""
    
    return np.sum( get_submatrix(M, R, C, d) )


def get_max_SMRC_with_conjecture(M, d):
    
    R, C = get_R_and_C_with_conjecture(M, d)
    
    return (get_SMRC(M, R, C, d), R, C)


## =========== Functions: get the true max(S_M(R,C)) =========== ##

# (These functions are using the ones above.)


def d_nested_for_loops(m_row, m_col, d):
    
    m_row_init = m_row
    m_col_init = m_col
    
    # loop on lines
    for i in range(n-m_row):
        
        m_row = m_row_init
        
        #print("m_row:", m_row)
        if m_row == 0:
            rows_indices_list[m_row] = deepcopy(rows_indices)
        else:
            rows_indices_list[m_row] = deepcopy(rows_indices_list[m_row-1])
        
        R[0][m_row] = rows_indices_list[m_row].pop(i)
        
        if m_row + 1 < d:
            
            m_row += 1
            d_nested_for_loops(m_row, m_col, d)
        
        else:                           # ie m_row = d
            
            #m_row = 0
        
            # loop on columns
            for j in range(n-m_col):
    
                m_col = m_col_init
                
                if m_col == 0:
                    cols_indices_list[m_col] = deepcopy(cols_indices)
                else:
                    cols_indices_list[m_col] = deepcopy(cols_indices_list[m_col-1])
                
                #print("m_col:", m_col)
                C[m_col][0] = cols_indices_list[m_col].pop(j)
                
                m_col += 1
                
                if m_col < d:
                    d_nested_for_loops(m_row, m_col, d)
                
                else:                           # ie m_col = d (and m_row = d)
                    
                    #m_col = 0
                    
                    SMRC = get_SMRC(M, R, C, d)
                    
                    if SMRC > best_SMRC_list[0]:
                        best_SMRC_list[0] = SMRC
                        best_R_list[0] = deepcopy(R)
                        best_C_list[0] = deepcopy(C)


def get_true_max_SMRC(M, d):
    
    global best_R_list, best_C_list
    global R, C
    global rows_indices, cols_indices
    global rows_indices_list, cols_indices_list
    global best_SMRC_list
    
    # a list because "global" keyword is not enough
    best_SMRC_list = [0]
    n = len(M)
    
    # lists to avoid having mute variable
    # in d_nested_for_loops()
    best_R_list = [-1]
    best_C_list = [-1]
    
    R = -1 * np.ones( (1,d) , dtype = int)
    C = -1 * np.ones( (d,1) , dtype = int)

    rows_indices = [ i for i in range(n) ]
    cols_indices = [ j for j in range(n) ]

    rows_indices_list = [ deepcopy(rows_indices) for m in range(d) ]
    cols_indices_list = [ deepcopy(cols_indices) for m in range(d) ]

    m_row = 0
    m_col = 0
    d_nested_for_loops(m_row, m_col, d)
                    
    return (best_SMRC_list[0], best_R_list[0], best_C_list[0])


def get_true_max_SMRC_old(M, d):
    
    best_SMRC = 0
    n = len(M)
    
    R = -1 * np.ones( (1,d) )
    C = -1 * np.ones( (d,1) )

    rows_indices = [ i for i in range(n) ]
    cols_indices = [ j for j in range(n) ]

    rows_indices_list = [ rows_indices for m in range(d) ]
    cols_indices_list = [ cols_indices for m in range(d) ]

    m = 0
    
    for i in range(n-m):
        
        m = 0
        
        rows_indices_list[m] = rows_indices
        cols_indices_list[m] = cols_indices
        
        R[0][m] = rows_indices_list[m].pop(i)
        C[m][0] = cols_indices_list[m].pop(i)
        
        m = 1
        
        for j in range(n-m):
            
            m = 1
            
            rows_indices_list[m] = rows_indices_list[m-1]
            cols_indices_list[m] = cols_indices_list[m-1]
            
            R[0][m] = rows_indices_list[m].pop(j)
            C[m][0] = cols_indices_list[m].pop(j)
            
            m = 2
            
            for k in range(n-m):
                
                m = 2
                
                rows_indices_list[m] = rows_indices_list[m-1]
                cols_indices_list[m] = cols_indices_list[m-1]
                
                R[0][m] = rows_indices.pop(k)
                C[m][0] = cols_indices.pop(k)
            
                SMRC = get_SMRC(M, R, C, d)
                
                if SMRC > best_SMRC:
                    best_SMRC = SMRC
                    best_R = deepcopy(R)
                    best_C = deepcopy(C)
                    
    return (best_SMRC, best_R, best_C)
    
    
    
    
    
    
    